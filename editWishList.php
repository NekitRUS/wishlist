<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="wishListCSS.css" type="text/css" rel="stylesheet" media="all" />
        <title></title>
    </head>
    <body>
        <?php
        session_start();
        if (array_key_exists("user", $_SESSION)) {
            echo "<p class='welcome'>Здравствуйте, " . $_SESSION['user']."</p>";
        }
        else {
            header('Location: index.php');
            exit;
        }
        ?>
        
        <table class="std">
            <tr><th>Item</th>
            <th>Due Date</th></tr>
            <?php
            require_once("Includes/db.php");
            $wisherID = WishDB::getInstance()->get_wisher_id_by_name($_SESSION["user"]);
            $result = WishDB::getInstance()->get_wishes_by_wisher_id($wisherID);
            while($row = mysqli_fetch_array($result)):
                echo "<tr><td>" . htmlentities($row['description']) . "</td>";
                echo "<td>" . htmlentities($row['due_date']) . "</td>";
                $wishID = $row["id"];
            ?>
            <td>
                <form name="editWish" action="editWish.php" method="GET">
                    <input type="hidden" name="wishID" value="<?php echo $wishID; ?>">
                    <input class="myButton" type="submit" name="editWish" value="Редактировать">
                </form>
            </td>
            <td>
                <form name="deleteWish" action="deleteWish.php" method="POST">
                    <input type="hidden" name="wishID" value="<?php echo $wishID; ?>"/>
                    <input class="myButton" type="submit" name="deleteWish" value="Удалить"/>
                </form>
            </td>
            <?php
            echo "</tr>\n";
            endwhile;
            mysqli_free_result($result);
            ?>
        </table>
        
        <form class="logon" name="addNewWish" action="editWish.php">            
            <input class="myButton" style="font-size: 18px" type="submit" value="Добавить желание">
        </form>
        
        <form class="logon" name="backToMainPage" action="index.php">
            <input class="logon_input" type="submit" value="На главную"/>
        </form>
    </body>
    </head>
</html>