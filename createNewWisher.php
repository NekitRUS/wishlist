<?php

require_once("Includes/db.php");

$userNameIsUnique = true;
$passwordIsValid = true;				
$userIsEmpty = false;					
$passwordIsEmpty = false;				
$password2IsEmpty = false;

if ($_SERVER["REQUEST_METHOD"] == "POST") {

    if ($_POST["user"]=="") {
        $userIsEmpty = true;
    }
    $wisherID = WishDB::getInstance()->get_wisher_id_by_name($_POST["user"]);
    if ($wisherID) {
        $userNameIsUnique = false;
    }
    
    if ($_POST["password"]=="") {
        $passwordIsEmpty = true;
    }
    if ($_POST["password2"]=="") {
        $password2IsEmpty = true;
    }
    if ($_POST["password"]!=$_POST["password2"]) {
        $passwordIsValid = false;
    } 
    
    if (!$userIsEmpty && $userNameIsUnique && !$passwordIsEmpty && !$password2IsEmpty && $passwordIsValid) {
        WishDB::getInstance()->create_wisher($_POST["user"], $_POST["password"]);
        session_start();
        $_SESSION['user'] = $_POST['user'];
        header('Location: editWishList.php' );
        exit;
    }
} 
?>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="wishListCSS.css" type="text/css" rel="stylesheet" media="all" />
        <title></title>
    </head>
    <body>
        <form class="showWishList" action="createNewWisher.php" method="POST">
            Your name: <input type="text" name="user"/><br/>
            <?php
            if ($userIsEmpty) {
                echo ("<p class='error'>Пожалуйста, введите имя!</p>");
            }                
            if (!$userNameIsUnique) {
                echo ("<p class='error'>Пользователь с таким именем уже существует. Пожалуйста, проверьте правильность ввода и попробуйте еще раз.</p>");
            }
            ?> 
            Password: <input type="password" name="password"/><br/>
            <?php
            if ($passwordIsEmpty) {
                echo ("<p class='error'>Пожалуйста, введите пароль!</p>");
            }                
            ?>
            Please confirm your password: <input type="password" name="password2"/><br/>
            <?php
            if ($password2IsEmpty) {
                echo ("<p class='error'>Пожалуйста, подтвердите пароль!</p>");
            }                
            if (!$password2IsEmpty && !$passwordIsValid) {
                echo  ("<p class='error'>Пароли не совпадают!</p>");
            }                 
           ?>
            <input type="submit" value="Зарегистрировать"/>
        </form>
    </body>
    </head>
</html>
