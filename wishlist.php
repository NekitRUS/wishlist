<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="wishListCSS.css" type="text/css" rel="stylesheet" media="all" />
        <title></title>
    </head>
    <body>
        <p class="welcome">Список желаний пользователя  <?php echo "'".htmlentities($_GET["user"])."'"."<br/>";?></p>
        <?php
        require_once("Includes/db.php");
        
        $wisherID = WishDB::getInstance()->get_wisher_id_by_name($_GET["user"]);
        if (!$wisherID) {
            exit("<p class='error'>Пользователь '" .$_GET["user"]. "' не найден. Пожалуйста, проверьте правильность ввода и попробуйте еще раз.</p>");
        }
        ?>
        <table class="std">
            <tr>
                <th>Item</th>
                <th>Due Date</th>
            </tr>
            <?php
                $result = WishDB::getInstance()->get_wishes_by_wisher_id($wisherID);
                while ($row = mysqli_fetch_array($result)) {
                    echo "<tr><td>" . htmlentities($row["description"]) . "</td>";
                    echo "<td>" . htmlentities($row["due_date"]) . "</td></tr>\n";
                }
                mysqli_free_result($result);
            ?>
        </table>
    </body>
</html>