<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<?php
    require_once("Includes/db.php");
    $loginSuccess = false;

    if ($_SERVER['REQUEST_METHOD'] == "POST") {
        $loginSuccess = (WishDB::getInstance()->verify_wisher_credentials($_POST['user'], $_POST['userpassword']));
        if ($loginSuccess == true) {
            session_start();
            $_SESSION['user'] = $_POST['user'];
            header('Location: editWishList.php');
            exit;
        }
    }
?>

<html>
    <head>
        <meta charset="UTF-8">
        <link href="wishListCSS.css" type="text/css" rel="stylesheet" media="all" />
        <title></title>
    </head>
    <body>
        <input class="logon" type="submit" name="showWishList" value="Список желаний пользователя >>" onclick="javascript:showHideShowWishListForm()"/>
        <form class="logon" name="wishList" action="wishlist.php" method="GET" style="visibility:hidden">
            <input class="logon_input" type="text" name="user" value="" />
            <input class="logon_input" type="submit" value="Показать" />
        </form>
        <br><H1>У вас до сих пор нет списка желаний?! <a href="createNewWisher.php">Создать сейчас</a></H1>
        <input class="logon" type="submit" name="myWishList" value="Мой список желаний >>" onclick="javascript:showHideLoginForm()"/>
        <form class="logon"  name="login" action="index.php" method="POST"
              style="visibility:<?php if ($loginSuccess) echo "hidden";
                                    else echo "visible";?>">
            Username: <input class="logon_input" type="text" name="user">
            Password: <input class="logon_input" type="password" name="userpassword">
            <div class="error">
            <?php
                if ($_SERVER["REQUEST_METHOD"] == "POST") { 
                    if (!$loginSuccess){
                        echo "Неверное имя пользователя и/или пароль!";
                    }
                }
              ?>
            </div>
            <input class="logon_input" type="submit" value="Редактировать мой список желаний"/>
        </form>
        
        
        <script>
        function showHideLoginForm() {
            if (document.all.login.style.visibility == "visible"){
                document.all.login.style.visibility = "hidden";
                document.all.myWishList.value = "Мой список желаний >>";
            } 
            else {
                document.all.login.style.visibility = "visible";
                document.all.myWishList.value = "<< Мой список желаний";
            }
        }
        
        function showHideShowWishListForm() {
            if (document.all.wishList.style.visibility == "visible") {
                document.all.wishList.style.visibility = "hidden";
                document.all.showWishList.value = "Список желаний пользователя >>";
            }
            else {
                document.all.wishList.style.visibility = "visible";
                document.all.showWishList.value = "<< Список желаний пользователя";
            }
        }
        </script> 
    </body>
</html>
